#include "collision_bug_plugin.h"

SimpleGazeboCollisionBugExample* door;

GZ_REGISTER_MODEL_PLUGIN(SimpleGazeboCollisionBugExample)

SimpleGazeboCollisionBugExample::SimpleGazeboCollisionBugExample(void){
  //empty constructor
}

SimpleGazeboCollisionBugExample::~SimpleGazeboCollisionBugExample( void ) {
    gazebo::event::Events::DisconnectWorldUpdateBegin( this->updateConnection );
}

void SimpleGazeboCollisionBugExample::Load(gazebo::physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  model = _model;
  world = _model->GetWorld();
  
  //---------------------------------------------------------------------------
  // Links
  //---------------------------------------------------------------------------
  
  links = model->GetLinks();

  door_link_ = model->GetLink("door_link");
  if( !door_link_ ) {
    gzerr << "Unable to find link: door_pannel_link\n";
    return;
  }
  
  lock_link_ = model->GetLink("lock_link");
  if( !lock_link_ ) {
    gzerr << "Unable to find link: lock_link\n";
    return;
  }

  //---------------------------------------------------------------------------
  // Joints
  //---------------------------------------------------------------------------
  
  joints = model->GetJoints();
  
  hinge_joint_ = model->GetJoint("hinge_joint");
  if( !hinge_joint_ ) {
    gzerr << "Unable to find joint: hinge_joint\n";
    return;
  }
  
  door = this;
  init();

  
  start_time = world->GetSimTime();
  last_time = start_time;
  
  std::cout<< "plugin loaded" << std::endl;
  std::cout<< "door_joint_ position "<< hinge_joint_->GetAngle( 0 ).Radian( )<< std::endl;
}

void SimpleGazeboCollisionBugExample::Update( ) {

  double hinge_joint_position = hinge_joint_->GetAngle( 0 ).Radian( );

  //this is so we can test the joints limits & hybrid transitions
  gazebo::common::Time sim_time = world->GetSimTime();
  double time = (sim_time - start_time).Double();
  // test the door hinge joint
  double hinge_joint_velocity = hinge_joint_->GetVelocity( 0 );
  double hinge_joint_torque = control(time, hinge_joint_position, hinge_joint_velocity, PI/4);
  hinge_joint_->SetForce( 0,hinge_joint_torque );

  last_time = sim_time;
   
  //print the link collision world poses
  print_collision_poses();
}

void SimpleGazeboCollisionBugExample::print_collision_poses()
{
  std::cout<< "door_joint_ position "<< hinge_joint_->GetAngle( 0 ).Radian( )<< std::endl;  

  gazebo::physics::Collision_V link_collisions;
  for (unsigned int j = 0; j < links.size(); ++j) 
  {
    link_collisions = links[j]->GetCollisions();  
    for(unsigned int i = 0; i < link_collisions.size(); ++i)
    {
      gazebo::math::Pose world_collision_pose = link_collisions[i]->GetWorldPose();
      std::cout<< link_collisions[i]->GetName() << " collision world pose "<< world_collision_pose << std::endl;
    }  
  }
}


void SimpleGazeboCollisionBugExample::init( void ) {
  this->updateConnection = gazebo::event::Events::ConnectWorldUpdateBegin(
    boost::bind( &SimpleGazeboCollisionBugExample::Update, this ) );
}


