#ifndef _GAZEBO_BUG_H_
#define _GAZEBO_BUG_H_
//gazebo
#include <gazebo/gazebo.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Events.hh>
#include <gazebo/physics/physics.hh>
//constants
#define PI 3.14159265359

class SimpleGazeboCollisionBugExample: public gazebo::ModelPlugin {

private:

  gazebo::event::ConnectionPtr updateConnection;

  gazebo::physics::ModelPtr model;
  gazebo::physics::WorldPtr world;
 

  gazebo::physics::Link_V links;
  
  gazebo::physics::LinkPtr door_link_;
  gazebo::physics::LinkPtr lock_link_;
   
  gazebo::physics::Joint_V joints; 

  gazebo::physics::JointPtr hinge_joint_;
  gazebo::physics::JointPtr lock_joint_;
  
  gazebo::common::Time start_time, last_time;  
  
  
public:
  //---------------------------------------------------------------------------
  // Constructors
  //---------------------------------------------------------------------------
  SimpleGazeboCollisionBugExample( void );
    
  //---------------------------------------------------------------------------
  // Destructor
  //---------------------------------------------------------------------------
  virtual ~SimpleGazeboCollisionBugExample( void );
  void print_collision_poses();

protected:
  //---------------------------------------------------------------------------
  // Gazebo ModelPlugin Interface
  //---------------------------------------------------------------------------
  virtual void Load( gazebo::physics::ModelPtr _model, sdf::ElementPtr _sdf );
  virtual void Update( );
  //virtual void Reset( );

  //---------------------------------------------------------------------------
  // Robot Interface
  //---------------------------------------------------------------------------
  virtual void init( void );
  
private:
  //-----------------------------------------------------------------------------
  // Trajectory Functions
  //-----------------------------------------------------------------------------

  /// The position trajectory function.  A cubic
  double position_trajectory( double t, double tfinal, double q0, double qdes ) {

      if( t > tfinal )
          return qdes;
      return -2 * (qdes - q0) / (tfinal*tfinal*tfinal) * (t*t*t) + 3 * (qdes - q0) / (tfinal*tfinal) * (t*t) + q0;
  }

  //-----------------------------------------------------------------------------

  /// The velocity trajectory function.  The first derivative of the cubic
  double velocity_trajectory( double t, double tfinal, double q0, double qdes ) {

      if( t > tfinal )
          return 0.0;
      return -6 * (qdes - q0) / (tfinal*tfinal*tfinal) * (t*t) + 6 * (qdes - q0) / (tfinal*tfinal) * (t);
  }

  /// Control function for Standalone Controller
  double control( double time, double position, double velocity, double angle) {
    //const Real Kp = 1.0;
    //const Real Kv = 0.1;

    const double Kp = 100.0;
    const double Kv = 10.0;

    double measured_position = position;
    double measured_velocity = velocity;

    //Real desired_position = position_trajectory( time, 0.5, 0.0, PI );
    //Real desired_velocity = velocity_trajectory( time, 0.5, 0.0, PI );

    double desired_position = position_trajectory( time, 10.0, 0.0, angle );
    double desired_velocity = velocity_trajectory( time, 10.0, 0.0, angle );

    double torque = Kp * ( desired_position - measured_position ) + Kv * ( desired_velocity - measured_velocity );

    return torque;
  }

  //-----------------------------------------------------------------------------
  // Publishing Functions
  //-----------------------------------------------------------------------------
  
  void init_collision_markers();
  void publishCollisionMarkers();
};

#endif //_GAZEBO_BUG_H_
